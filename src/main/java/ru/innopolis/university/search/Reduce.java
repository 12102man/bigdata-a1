package ru.innopolis.university.search;

import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static ru.innopolis.university.Search.LIMIT;

public class Reduce extends Reducer<LongWritable, MapWritable, Text, DoubleWritable> {

    @Override
    protected void reduce(LongWritable key, Iterable<MapWritable> values, Context context) throws IOException, InterruptedException {
        Map<String, Double> scores = new LinkedHashMap<>();
        for (MapWritable map : values) {
            Map.Entry<Writable, Writable> entry = map.entrySet().stream().findFirst().orElseThrow(RuntimeException::new);
            scores.put(entry.getKey().toString(), Double.parseDouble(entry.getValue().toString()));
        }

        long limit = context.getConfiguration().getLong(LIMIT, -1);

        if (limit == -1) {
            limit = scores.size();
        }

        scores = scores.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .limit(limit)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));

        for (Map.Entry<String, Double> entry :
                scores.entrySet()) {
            context.write(new Text(entry.getKey()), new DoubleWritable(entry.getValue()));
        }
    }
}
