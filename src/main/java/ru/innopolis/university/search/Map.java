package ru.innopolis.university.search;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import ru.innopolis.university.Parser;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import static ru.innopolis.university.Search.QUERY;

public class Map extends Mapper<Text, Text, LongWritable, MapWritable> {

    @Override
    protected void map(Text title, Text tfIdfJson, Context context) throws IOException, InterruptedException {
        java.util.Map<String, Double> tfIdf = Parser.getMapFromJsonDouble(tfIdfJson.toString());
        java.util.Map<String, Double> idf = Parser.getIdf(context.getConfiguration());

        Set<String> query = Arrays.stream(context.getConfiguration().get(QUERY).split(" "))
                .map(term -> term.replaceAll("\\W", "").toLowerCase())
                .collect(Collectors.toSet());
        Set<String> tfIdfKeys = tfIdf.keySet();
        tfIdfKeys.retainAll(query);

        double score = 0.0;
        for (String key: tfIdfKeys) {
            score += tfIdf.get(key) * ((double) Collections.frequency(query, key) / idf.get(key));
        }

        MapWritable result = new MapWritable();
        result.put(title, new DoubleWritable(score));
        context.write(new LongWritable(1), result);
    }
}
