package ru.innopolis.university.number_of_documents;

import com.google.gson.Gson;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import ru.innopolis.university.WikiArticle;

import java.io.IOException;

public class Map extends Mapper<LongWritable, Text, LongWritable, Text> {

    @Override
    protected void map(LongWritable key, Text json, Mapper<LongWritable, Text, LongWritable, Text>.Context context)
            throws IOException, InterruptedException {
        Gson gson = new Gson();
        WikiArticle parsed = gson.fromJson(json.toString(), WikiArticle.class);
        String[] tokens = parsed.getText().split(" ");
        String title = parsed.getTitle();
        context.write(new LongWritable(1), new Text(title));
    }
}