package ru.innopolis.university.number_of_documents;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.io.OutputStream;
import java.util.stream.StreamSupport;

import static ru.innopolis.university.Parser.NUMBER_OF_DOCUMENTS_TXT;

public class Reduce extends Reducer<LongWritable, Text, Text, LongWritable> {

    @Override
    protected void reduce(LongWritable key, Iterable<Text> documentTitles, Reducer<LongWritable, Text, Text, LongWritable>.Context context)
            throws IOException, InterruptedException {
        Configuration conf = context.getConfiguration();
        FileSystem fs = FileSystem.get(conf);
        OutputStream os = fs.create(new Path(NUMBER_OF_DOCUMENTS_TXT));

        long number_of_documents = StreamSupport.stream(documentTitles.spliterator(), false).count();

        os.write(Long.toString(number_of_documents).getBytes());
        context.write(
                new Text("N"),
                new LongWritable(number_of_documents)
        );
    }
}
