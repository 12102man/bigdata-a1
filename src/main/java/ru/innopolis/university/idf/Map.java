package ru.innopolis.university.idf;

import com.google.gson.Gson;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import ru.innopolis.university.WikiArticle;

import java.io.IOException;

public class Map extends Mapper<LongWritable, Text, Text, Text> {

    @Override
    protected void map(LongWritable key, Text json, Mapper<LongWritable, Text, Text, Text>.Context context)
            throws IOException, InterruptedException {
        Gson gson = new Gson();
        WikiArticle parsed = gson.fromJson(json.toString(), WikiArticle.class);
        String[] tokens = parsed.getText().split(" ");
        String title = parsed.getTitle();
        for (String string : tokens) {
            context.write(new Text(string.replaceAll("\\W", "").toLowerCase()), new Text(title));
        }
    }
}