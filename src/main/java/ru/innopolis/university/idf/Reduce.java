package ru.innopolis.university.idf;


import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.io.InputStream;
import java.util.stream.StreamSupport;

import static ru.innopolis.university.Parser.NUMBER_OF_DOCUMENTS_TXT;

public class Reduce extends Reducer<Text, Text, Text, DoubleWritable> {

    @Override
    protected void reduce(Text term, Iterable<Text> documentTitles, Reducer<Text, Text, Text, DoubleWritable>.Context context)
            throws IOException, InterruptedException {
        FileSystem fs = FileSystem.get(context.getConfiguration());
        InputStream is = fs.open(new Path(NUMBER_OF_DOCUMENTS_TXT));
        long n = is.read();
        context.write(term, new DoubleWritable(
                Math.log((double) n / StreamSupport.stream(documentTitles.spliterator(), false)
                        .distinct()
                        .count())));
    }
}
