package ru.innopolis.university;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import ru.innopolis.university.search.Map;
import ru.innopolis.university.search.Reduce;

import static ru.innopolis.university.Parser.*;

public class Search extends Configured implements Tool {

    public static final String QUERY = "query";
    public static final String LIMIT = "limit";

    @SuppressWarnings("deprecation")
    public int run(String[] args) throws Exception {
        Configuration conf = new Configuration();

        String[] paths = {RESULT};

        FileSystem hdfs = FileSystem.get(conf);
        for (String path : paths) {
            if (hdfs.exists(new Path(path))) {
                hdfs.delete(new Path(path), true);
            }
        }

        conf.set(QUERY, args[0]);
        conf.setLong(LIMIT, Long.parseLong(args[1]));

        Job jobSearch = new Job(conf, "Search");
        jobSearch.setJarByClass(this.getClass());
        jobSearch.setMapperClass(Map.class);
        jobSearch.setReducerClass(Reduce.class);
        jobSearch.setInputFormatClass(KeyValueTextInputFormat.class);
        jobSearch.setMapOutputKeyClass(LongWritable.class);
        jobSearch.setMapOutputValueClass(MapWritable.class);
        FileInputFormat.addInputPath(jobSearch, new Path(TF_IDF));
        FileOutputFormat.setOutputPath(jobSearch, new Path(RESULT));
        jobSearch.setOutputKeyClass(Text.class);
        jobSearch.setOutputValueClass(DoubleWritable.class);

        return jobSearch.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        int jobStatus = ToolRunner.run(new Search(), args);
        System.exit(jobStatus);
    }
}