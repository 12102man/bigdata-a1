package ru.innopolis.university.tf_idf;

import com.google.gson.Gson;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import ru.innopolis.university.Parser;

import java.io.IOException;
import java.util.HashMap;

public class Map extends Mapper<Text, Text, Text, Text> {

    @Override
    protected void map(Text title, Text tfText, Context context) throws IOException, InterruptedException {
        java.util.Map<String, Long> tf = Parser.getMapFromJsonLong(tfText.toString());
        java.util.Map<String, Double> idf = Parser.getIdf(context.getConfiguration());

        java.util.Map<String, Double> tfIdf = new HashMap<>();
        for (java.util.Map.Entry<String, Long> entry : tf.entrySet()) {
            tfIdf.put(entry.getKey(), entry.getValue() / idf.get(entry.getKey()));
        }

        context.write(new Text(title), new Text(new Gson().toJson(tfIdf)));
    }
}
