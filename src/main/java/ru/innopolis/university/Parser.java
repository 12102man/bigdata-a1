package ru.innopolis.university;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class Parser {

    public static final String NUMBER_OF_DOCUMENTS = "/number_of_documents";
    public static final String NUMBER_OF_DOCUMENTS_TXT = "/number_of_documents.txt";
    public static final String DOCUMENTS_TF = "/documents_tf";
    public static final String IDF = "/idf";
    public static final String TF_IDF = "/tf_idf";
    public static final String RESULT = "/result";
    public static final String FILE = "/part-r-00000";

    public static Map<String, Double> getIdf(Configuration configuration) throws IOException {
        FileSystem fs = FileSystem.get(configuration);
        FSDataInputStream is = fs.open(new Path(IDF + FILE));
        java.util.Map<String, Double> idf = new HashMap<>();
        for (String line : IOUtils.readLines(is, "UTF-8")) {
            String[] idfEntry = line.split("\t");
            idf.put(idfEntry[0], Double.valueOf(idfEntry[1]));
        }
        return idf;
    }

    public static Map<String, Double> getMapFromJsonDouble(String json) {
        Type type = new TypeToken<Map<String, Double>>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }

    public static Map<String, Long> getMapFromJsonLong(String json) {
        Type type = new TypeToken<Map<String, Long>>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }
}
