package ru.innopolis.university.tf;

import com.google.gson.Gson;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Reduce extends Reducer<Text, Text, Text, Text> {

    @Override
    protected void reduce(Text key, Iterable<Text> words, Reducer<Text, Text, Text, Text>.Context context)
            throws IOException, InterruptedException {
        Map<String, Long> wordsCount = new HashMap<>();

        for (Text word : words) {
            if (!wordsCount.containsKey(word.toString())) {
                wordsCount.put(word.toString(), 1L);
            } else {
                long prev = wordsCount.get(word.toString());
                wordsCount.put(new Text(word.toString()).toString(), prev + 1);
            }
        }

        Gson gson = new Gson();

        context.write(key, new Text(gson.toJson(wordsCount)));

    }
}