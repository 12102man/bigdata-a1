package ru.innopolis.university;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.ID;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import static ru.innopolis.university.Parser.*;

public class Index extends Configured implements Tool {

    @SuppressWarnings("deprecation")
    public int run(String[] args) throws Exception {
        Configuration conf = new Configuration();

        String[] paths = {NUMBER_OF_DOCUMENTS, NUMBER_OF_DOCUMENTS_TXT, DOCUMENTS_TF, IDF, TF_IDF};

        FileSystem hdfs = FileSystem.get(conf);
        for (String path : paths) {
            if (hdfs.exists(new Path(path))) {
                hdfs.delete(new Path(path), true);
            }
        }


        Job jobNumberOfDocuments = new Job(conf, "Counting number of documents");
        jobNumberOfDocuments.setJarByClass(getClass());
        jobNumberOfDocuments.setMapperClass(ru.innopolis.university.number_of_documents.Map.class);
        jobNumberOfDocuments.setReducerClass(ru.innopolis.university.number_of_documents.Reduce.class);
        jobNumberOfDocuments.setMapOutputKeyClass(LongWritable.class);
        jobNumberOfDocuments.setMapOutputValueClass(Text.class);
        FileInputFormat.addInputPath(jobNumberOfDocuments, new Path(args[0]));
        FileOutputFormat.setOutputPath(jobNumberOfDocuments, new Path(NUMBER_OF_DOCUMENTS));
        jobNumberOfDocuments.setOutputKeyClass(Text.class);
        jobNumberOfDocuments.setOutputValueClass(LongWritable.class);
        int jobWaiter = jobNumberOfDocuments.waitForCompletion(true) ? 0 : 1;

        Job jobTfDocuments = new Job(conf, "Counting documents TF");
        jobTfDocuments.setJarByClass(getClass());
        jobTfDocuments.setMapperClass(ru.innopolis.university.tf.Map.class);
        jobTfDocuments.setReducerClass(ru.innopolis.university.tf.Reduce.class);
        jobTfDocuments.setMapOutputKeyClass(Text.class);
        jobTfDocuments.setMapOutputValueClass(Text.class);
        FileInputFormat.addInputPath(jobTfDocuments, new Path(args[0]));
        FileOutputFormat.setOutputPath(jobTfDocuments, new Path(DOCUMENTS_TF));
        jobTfDocuments.setOutputKeyClass(Text.class);
        jobTfDocuments.setOutputValueClass(Text.class);
        jobWaiter = jobTfDocuments.waitForCompletion(true) ? 0 : 1;

        Job jobIdf = new Job(conf, "Counting term IDF");
        jobIdf.setJarByClass(getClass());
        jobIdf.setMapperClass(ru.innopolis.university.idf.Map.class);
        jobIdf.setReducerClass(ru.innopolis.university.idf.Reduce.class);
        jobIdf.setMapOutputKeyClass(Text.class);
        jobIdf.setMapOutputValueClass(Text.class);
        FileInputFormat.addInputPath(jobIdf, new Path(args[0]));
        FileOutputFormat.setOutputPath(jobIdf, new Path(IDF));
        jobIdf.setOutputKeyClass(Text.class);
        jobIdf.setOutputValueClass(DoubleWritable.class);

        jobWaiter = jobIdf.waitForCompletion(true) ? 0 : 1;

        Job jobTfIdf = new Job(conf, "Counting term TF/IDF for documents");
        jobTfIdf.setJarByClass(getClass());
        jobTfIdf.setMapperClass(ru.innopolis.university.tf_idf.Map.class);
        jobTfIdf.setReducerClass(ru.innopolis.university.tf_idf.Reduce.class);
        jobTfIdf.setInputFormatClass(KeyValueTextInputFormat.class);
        jobTfIdf.setMapOutputKeyClass(Text.class);
        jobTfIdf.setMapOutputValueClass(Text.class);
        FileInputFormat.addInputPath(jobTfIdf, new Path(DOCUMENTS_TF));
        FileOutputFormat.setOutputPath(jobTfIdf, new Path(TF_IDF));
        jobTfIdf.setOutputKeyClass(Text.class);
        jobTfIdf.setOutputValueClass(Text.class);

        jobWaiter = jobTfIdf.waitForCompletion(true) ? 0 : 1;
        return jobWaiter;

    }

    public static void main(String[] args) throws Exception {
        int jobStatus = ToolRunner.run(new Index(), args);
        System.exit(jobStatus);
    }
}